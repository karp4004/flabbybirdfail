				UIntValuesArray& indices_array = meshPrimitive->getNormalIndices();				
				normal_out<<"normal indices: "<<indices_array.getCount()<<endl;
				for(int i=0;i<indices_array.getCount();i++)
				{
					normal_out<<indices_array.getData()[i]<<endl;
				}

				indices_array = meshPrimitive->getPositionIndices();				
				position_out<<"position indices: "<<indices_array.getCount()<<endl;
				for(int i=0;i<indices_array.getCount();i++)
				{
					position_out<<indices_array.getData()[i]<<endl;
				}

				indices_array = meshPrimitive->getUVCoordIndicesArray().getData()[0]->getIndices();				
				uv_out<<"uvs indices: "<<indices_array.getCount()<<endl;
				for(int i=0;i<indices_array.getCount();i++)
				{
					uv_out<<indices_array.getData()[i]<<endl;
				}
