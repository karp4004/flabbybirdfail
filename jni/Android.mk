LOCAL_PATH:= $(call my-dir)
 
include $(CLEAR_VARS)

LOCAL_CFLAGS := -Werror -std=c++0x $(LOCAL_C_INCLUDES:%=-I%) -DUSE_PTHREADS -mfpu=neon -mfloat-abi=softfp -pthread -DSCE_PFX_USE_SIMD_VECTORMATH
LOCAL_ARM_NEON := true
TARGET_CFLAGS := $(filter-out -ffpu=vfp,$(TARGET_CFLAGS))
# add the following #define to run standalone NEON speedup potential tests
# -DTEST_NEON_PERFORMANCE
# apply these flags if needed 
# -ffast-math -funsafe-math-optimizations
# apply this to disable optimization
# TARGET_CFLAGS := $(TARGET_CFLAGS) -O0

LOCAL_MODULE    := FBJNI
LOCAL_C_INCLUDES := $(LOCAL_PATH)/fb
LOCAL_SRC_FILES := fb/FBJNI.cpp \
				   fb/FlabbyBird.cpp \
				   fb/Math/Vector3.cpp \
				   fb/Math/Vector4.cpp \
				   fb/Math/Vector2.cpp \
				   fb/Math/Matrix4.cpp \
				   fb/Math/GEMath.cpp \
				   fb/PosixTimer.cpp \
				   fb/Timer.cpp \
				   fb/Logger.cpp \
				   fb/MeshNode.cpp \
				   fb/Texture.cpp \
				   fb/Barrier.cpp \
				   fb/Bird.cpp \
				   fb/rc.cpp \
				   fb/Shader.cpp
				   
LOCAL_LDLIBS    := -llog -lGLESv2 -lEGL -landroid -lOpenSLES

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/cpufeatures)