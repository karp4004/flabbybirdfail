#ifndef SHADERH
#define SHADERH

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include "Math/Matrix4.h"
#include "MeshNode.h"
#include "Texture.h"

class Shader
{
public:
	Shader();
	int onSurfaceCreated();
	int onSurfaceChanged(int l, int t, int width, int height, float coeff);
	void setAttributes(MeshNodeData* mesh);
	void RenderMesh(MeshNode* mesh);
	void setViewMatrix(Vector3 eye, Vector3 center, Vector3 up);

private:
	Matrix4 mViewMatrix;
	Matrix4 mProjectionMatrix;
	Matrix4 mMVMatrix;
	Matrix4 mMVPMatrix;

	GLuint mMVPMatrixHandle;

	int mBytesPerFloat;
	int mPositionDataSize;
	int mUVDataSize;
	int mNormalDataSize;

    GLint mSampler2D;
    GLuint mProgram;
	GLuint mPositionHandle;
	GLuint mNormalHandle;
	GLuint mUVHandle;
};

#endif
