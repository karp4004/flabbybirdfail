#ifndef TimerH
#define TimerH

#include <string>

using namespace std;

class Timer
{
public:
	Timer(string name);

	double GetElapsed();
	double GetDelta();
	virtual int Update() = 0;
	virtual int Reset() = 0;
	void ResetElapsed();

protected:
	void CountElapsed(double t);
	void SetDelta(double t);

private:
	double mElapsed;
	double mDelta;
};

#endif
