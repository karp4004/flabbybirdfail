#include "Texture.h"
#include <stdlib.h>
#include "Logger.h"

int glerror(const char* op) {

	int res = 0;
    for (GLint error = glGetError(); error; error
            = glGetError()) {
        res  =1;
    	LOGI("after %s() glError (0x%x)\n", op, error);
    }

    return res;
}

Texture::Texture(unsigned char* d, int sz, int w, int h):
mId(-1)
,mBpp(GL_RGBA)
,mFormat(GL_RGBA)
,mUnit(GL_TEXTURE0)
,mCap(GL_TEXTURE_2D)
{
	mWidth = w;
	mHeight = h;
	mDataSize = sz;
	mData = d;

	mData = new unsigned char[mDataSize];
	memcpy(mData, d, mDataSize);

	LOGT("", GetName().c_str());

   GLboolean isTex = glIsTexture(mId);
   if(isTex)
   {
	   LOGI("isTex:%d:%s", isTex, GetName().c_str());
   }

//	   if(mId!=-1)
//		   return 0;

   // Use tightly packed data
   glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 );
		glerror("glPixelStorei");

   // Generate a texture object
   glGenTextures ( 1, &mId );
		glerror("glGenTextures");

   // Bind the texture object
   glBindTexture ( mCap, mId );

		glerror("glBindTexture");

		LOGI("mFormat:%d", mFormat);
	   LOGI("width:%d", mWidth);
	   LOGI("height:%d", mHeight);
		LOGI("data_size:%d", mDataSize);
		LOGI("data:%p", mData);
		LOGI("GL_MAX_TEXTURE_SIZE:%d", GL_MAX_TEXTURE_SIZE);

		   glTexImage2D ( GL_TEXTURE_2D, 0, mFormat, mWidth, mHeight, 0, mFormat, GL_UNSIGNED_BYTE, mData );
		glerror("glTexImage2D");

   // Set the filtering mode
   glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST  );//GL_LINEAR
		glerror("glTexParameteri");

   glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST  );//GL_TEXTURE_2D
	glerror("glTexParameteri");
}

int Texture::Bind(GLint mSampler2D)
{
	LOGT("", GetName().c_str());

    glActiveTexture(mUnit);//GL_TEXTURE0
	glerror("glActiveTexture");

    glBindTexture(mCap, mId);//GL_TEXTURE_2D
	glerror("glBindTexture");

    glUniform1i(mSampler2D, 0); //Texture unit 0 is for base images.	return 0;
	glerror("mSampler2D");

	return 0;
}
