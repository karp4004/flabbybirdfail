#ifndef PosixTimerH
#define PosixTimerH

#include <time.h>
#include "Timer.h"
#include <string>

using namespace std;

class PosixTimer: public Timer
{
public:
	PosixTimer(string name);
	virtual int Update();
	virtual int Reset();

private:
	timespec old_t;
};

#endif
