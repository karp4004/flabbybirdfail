#ifndef MESHNODEH
#define MESHNODEH

#include "Texture.h"
#include <bits/extc++.h>
#include "Math/Matrix4.h"
#include "Math/Vector2.h"

using namespace std;

class Shader;

struct MeshNodeData
{
	float *positions, *normals, *uvs;
	int tris;
	shared_ptr<Texture> mTexture;
	float width;
	float height;
};

class MeshNode
{
public:
	MeshNode(MeshNodeData* data, Shader* shader);
	Vector2 GetPsition();
	MeshNodeData* getVB();
	Matrix4& pose();
	virtual int setPosition(Vector2 v);
	virtual int setScale(Vector2 v);

protected:
    Matrix4 mModelMatrix;
    MeshNodeData* mesh;
    Shader* mShader;
};


#endif
