#include "GEMath.h"
#include <algorithm>
#include "Logger.h"

#define DEBUG 0

Matrix4 frustumM(float left, float right, float bottom, float top, float near, float far)
{
//	if (left == right) {
//		throw new IllegalArgumentException("left == right");
//	}
//	if (top == bottom) {
//		throw new IllegalArgumentException("top == bottom");
//	}
//	if (near == far) {
//		throw new IllegalArgumentException("near == far");
//	}
//	if (near <= 0.0f) {
//		throw new IllegalArgumentException("near <= 0.0f");
//	}
//	if (far <= 0.0f) {
//		throw new IllegalArgumentException("far <= 0.0f");
//	}

	float r_width  = 1.0f / (right - left);
	float r_height = 1.0f / (top - bottom);
	float r_depth  = 1.0f / (near - far);
	float x = 2.0f * (near * r_width);
	float y = 2.0f * (near * r_height);
	float A = 2.0f * ((right + left) * r_width);
	float B = (top + bottom) * r_height;
	float C = (far + near) * r_depth;
	float D = 2.0f * (far * near * r_depth);

	Matrix4 m;

	m.v[0] = x;
	m.v[5] = y;
	m.v[8] = A;
	m.v[9] = B;
	m.v[10] = C;
	m.v[14] = D;
	m.v[11] = -1.0f;
	m.v[1] = 0.0f;
	m.v[2] = 0.0f;
	m.v[3] = 0.0f;
	m.v[4] = 0.0f;
	m.v[6] = 0.0f;
	m.v[7] = 0.0f;
	m.v[12] = 0.0f;
	m.v[13] = 0.0f;
	m.v[15] = 0.0f;

	return m;
}

Matrix4 getPerspective(float fovy, float aspect, float nearZ, float farZ)
{
   float frustumW, frustumH;

   frustumH = tanf( fovy / 360.0f * PI ) * nearZ;
   frustumW = frustumH * aspect;

   return frustumM( -frustumW, frustumW, -frustumH, frustumH, nearZ, farZ );
}

Matrix4 getOrtho(float left, float right, float bottom, float top, float near, float far)
{
    float       deltaX = right - left;
    float       deltaY = top - bottom;
    float       deltaZ = far - near;

    Matrix4    ortho = matrixIdentity();

    if ( (deltaX != 0.0f) || (deltaY != 0.0f) || (deltaZ != 0.0f) )
    {
        ortho.v[0] = 2.0f / deltaX;
        ortho.v[12] = -(right + left) / deltaX;
        ortho.v[5] = 2.0f / deltaY;
        ortho.v[13] = -(top + bottom) / deltaY;
        ortho.v[10] = -2.0f / deltaZ;
        ortho.v[14] = -(near + far) / deltaZ;
    }

    return ortho;
}

Matrix4 getLookAtM(Vector3& eye, Vector3& center, Vector3& up)
{
	Vector3 forward = center - eye;

	LOGOBJECT(forward, __FILE__, "forward");

	forward = normalize(forward);
	LOGOBJECT(forward, __FILE__, "forward");

	Vector3 side = cross(forward, up);
	LOGOBJECT(side, __FILE__, "side");

	side = normalize(side);
	LOGOBJECT(side, __FILE__, "side");

	up = cross(side, forward);
	LOGOBJECT(up, __FILE__, "up");


	Matrix4 m;
	m.v[0] = side.x;
	m.v[1] = up.x;
	m.v[2] = -forward.x;
	m.v[3] = 0.0f;

	m.v[4] = side.y;
	m.v[5] = up.y;
	m.v[6] = -forward.y;
	m.v[7] = 0.0f;

	m.v[8] = side.z;
	m.v[9] = up.z;
	m.v[10] = -forward.z;
	m.v[11] = 0.0f;

	m.v[12] = 0.0f;
	m.v[13] = 0.0f;
	m.v[14] = 0.0f;
	m.v[15] = 1.0f;

	m = translateM(m, -eye.x, -eye.y, -eye.z);

	return m;
}
