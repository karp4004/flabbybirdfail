#include "Vector2.h"

#include "math.h"

#define DEBUG 1
#include "Logger.h"

Vector2 GenerateVector(float x, float y)
{
	Vector2 ret;
	ret.x = x;
	ret.y = y;
	return ret;
}

float VL(Vector2& vec)
{
	return VectorLength(vec);
}

int Vector2::Log(string module, string name)
{
	LOGD("%f, %f module:%s name:%s", x, y, module.c_str(), name.c_str());
	return 0;
}

Vector2 GV(float x, float y)
{
	return GenerateVector(x, y);
}

Vector2 operator-(Vector2 vec1, Vector2 vec2)
{
	Vector2 res;

	res.x = vec1.x - vec2.x;
	res.y = vec1.y - vec2.y;

	return res;
}

Vector2 operator+(Vector2 vec1, Vector2 vec2)
{
	Vector2 res;

	res.x = vec1.x + vec2.x;
	res.y = vec1.y + vec2.y;

	return res;
}

Vector2 normalize(Vector2& vec)
{
	float length = VectorLength(vec);
	vec = vec / length;
	return vec;
}

float VectorLength(Vector2& vec)
{
	float length = vec.x*vec.x + vec.y*vec.y;
	length = sqrt(length);

	return length;
}

Vector2 operator/(Vector2 vec1, float scalar)
{
	Vector2 res;

	res.x = vec1.x/scalar;
	res.y = vec1.y/scalar;

	return res;
}

Vector2 operator*(Vector2 vec1, float scalar)
{
	Vector2 res;

	res.x = vec1.x*scalar;
	res.y = vec1.y*scalar;

	return res;
}

float dot(Vector2& vec1, Vector2& vec2)
{
	float res;
	res = vec1.x*vec2.x + vec1.y*vec2.y;
	return res;
}

#define norm(v)    sqrt(dot(v,v))  // norm = length of vector
#define d(u,v)     norm(u-v)

float vdistance(Vector2& vec1, Vector2& vec2)
{
	Vector2 ret = vec1 - vec2;
	return VL(ret);
}

float distance_Point_to_Segment(Vector2 P, Vector2 P0, Vector2 P1)
{
    Vector2 v = P1 - P0;
    Vector2 w = P - P0;

    double c1 = dot(w,v);
    if ( c1 <= 0 )
        return vdistance(P, P0);

    double c2 = dot(v,v);
    if ( c2 <= c1 )
        return vdistance(P, P1);

    double b = c1 / c2;
    v = v*b;
    Vector2 Pb = P0 + v;

    return vdistance(P, Pb);
}
