#ifndef Vector3H
#define Vector3H

#include <string>

using namespace std;

class Vector4;

struct Vector3
{
	float x,y,z;

	int Log(string module);
	int Log(string module, string name);
};


Vector3 NewVec3(Vector4& v4);
Vector3 GenerateVector(float x, float y, float z);
Vector3 GV(float x, float y, float z);
float VectorLength(Vector3& vec);
float VL(Vector3& vec);
Vector3 operator/(Vector3 vec1, float scalar);
Vector3 operator*(Vector3 vec1, float scalar);
Vector3 operator-(Vector3 vec1, Vector3 vec2);
Vector3 operator-(Vector3 vec1);
Vector3 operator+(Vector3 vec1, Vector3 vec2);
Vector3 normalize(Vector3& vec);
float dot(Vector3& vec1, Vector3& vec2);
Vector3 cross(Vector3& vec1, Vector3& vec2);
float VA(Vector3& vec1, Vector3& vec2);

#endif
