#ifndef GEMathH
#define GEMathH

#include "math.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Vector4.h"
#include "Matrix4.h"

#define PI 3.1415926535897932384626433832795f
// Pre-calculated value of PI / 180.
#define kPI180   0.017453

// Pre-calculated value of 180 / PI.
#define k180PI  57.295780

// Converts degrees to radians.
#define degreesToRadians(x) (x * kPI180)

// Converts radians to degrees.
#define radiansToDegrees(x) (x * k180PI)

Matrix4 getLookAtM(Vector3& eye, Vector3& center, Vector3& up);
Matrix4 getPerspective(float fovy, float aspect, float nearZ, float farZ);
Matrix4 getOrtho(float left, float right, float bottom, float top, float nearZ, float farZ);

#endif
