#ifndef Vector2H
#define Vector2H

#include <string>

using namespace std;

struct Vector2
{
	float x,y;

	int Log(string module, string name);
};

Vector2 GenerateVector(float x, float y);
Vector2 GV(float x, float y);
float VL(Vector2& vec);
Vector2 operator-(Vector2 vec1, Vector2 vec2);
Vector2 operator+(Vector2 vec1, Vector2 vec2);
Vector2 normalize(Vector2& vec);
float VectorLength(Vector2& vec);
Vector2 operator/(Vector2 vec1, float scalar);
Vector2 operator*(Vector2 vec1, float scalar);
float dot(Vector2& vec1, Vector2& vec2);
float vdistance(Vector2& vec1, Vector2& vec2);
float distance_Point_to_Segment(Vector2 P, Vector2 P0, Vector2 P1);

#endif
