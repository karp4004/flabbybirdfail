#include "Math/Vector4.h"
#include "Math/Vector3.h"
#include "Logger.h"

Vector4::Vector4()
{
	x = 0.;
	y = 0.;
	z = 0.;
	w = 0.;
}

Vector4::Vector4(Vector3& v3)
{
	x = v3.x;
	y = v3.y;
	z = v3.z;
	w = 1.0;
}

Vector4 GenerateVector(float x, float y, float z, float w)
{
	Vector4 result;

	result.x = x;
	result.y = y;
	result.z = z;
	result.w = w;

	return result;
}

Vector4 GV(float x, float y, float z, float w)
{
	return GenerateVector(x, y, z, w);
}
