#include "math.h"
#include "Vector3.h"
#include "Math/Vector4.h"
#include "Math/GEMath.h"

#define DEBUG 1
#include "Logger.h"

Vector3 NewVec3(Vector4& v4)
{
	Vector3 v3;
	v3.x = v4.x;
	v3.y = v4.y;
	v3.z = v4.z;

	return v3;
}

Vector3 GenerateVector(float x, float y, float z)
{
	Vector3 res;

	res.x = x;
	res.y = y;
	res.z = z;

	return res;
}

Vector3 GV(float x, float y, float z)
{
	return GenerateVector(x, y, z);
}

float VectorLength(Vector3& vec)
{
	float length = vec.x*vec.x + vec.y*vec.y + vec.z*vec.z;
	length = sqrt(length);

	return length;
}

float VL(Vector3& vec)
{
	return VectorLength(vec);
}

Vector3 operator/(Vector3 vec1, float scalar)
{
	Vector3 res;

	res.x = vec1.x/scalar;
	res.y = vec1.y/scalar;
	res.z = vec1.z/scalar;

	return res;
}

Vector3 operator*(Vector3 vec1, float scalar)
{
	Vector3 res;

	res.x = vec1.x*scalar;
	res.y = vec1.y*scalar;
	res.z = vec1.z*scalar;

	return res;
}

Vector3 operator-(Vector3 vec1)
{
	vec1.x = -vec1.x;
	vec1.y = -vec1.y;
	vec1.z = -vec1.z;

	return vec1;
}

Vector3 operator-(Vector3 vec1, Vector3 vec2)
{
	Vector3 res;

	res.x = vec1.x - vec2.x;
	res.y = vec1.y - vec2.y;
	res.z = vec1.z - vec2.z;

	return res;
}

Vector3 operator+(Vector3 vec1, Vector3 vec2)
{
	Vector3 res;

	res.x = vec1.x + vec2.x;
	res.y = vec1.y + vec2.y;
	res.z = vec1.z + vec2.z;

	return res;
}

Vector3 normalize(Vector3& vec)
{
	float length = VectorLength(vec);
	vec = vec / length;
	return vec;
}

float dot(Vector3& vec1, Vector3& vec2)
{
	float res;
	res = vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z;
	return res;
}

Vector3 cross(Vector3& vec1, Vector3& vec2)
{
	Vector3 res;

	res.x = vec1.y*vec2.z - vec1.z*vec2.y;
	res.y = vec1.z*vec2.x - vec1.x*vec2.z;
	res.z = vec1.x*vec2.y - vec1.y*vec2.x;

	return res;
}

float VA(Vector3& vec1, Vector3& vec2)
{
	float a_mag = VL(vec1);
	float b_mag = VL(vec2);
	float ab_dot = dot(vec1, vec2);
	float c = ab_dot / (a_mag * b_mag);

	// clamp d to from going beyond +/- 1 as acos(+1/-1) results in infinity
//	if (c > 1.0f) {
//	    c = 1.0;
//	} else if (c < -1.0) {
//	    c = -1.0;
//	}
//
//	c = acos(c);
//	c *= k180PI;


	c = atan2(vec1.x*vec2.z - vec2.x*vec1.z, vec1.x*vec2.x + vec1.z*vec2.z);
//	c = atan2(VL(cross(vec1, vec2)), dot(vec1, vec2));

		c *= k180PI;

	return c;
}

int Vector3::Log(string module)
{
	LOGD("%f, %f, %f module:%s", x, y, z, module.c_str());
	return 0;
}

int Vector3::Log(string module, string name)
{
	LOGD("%f, %f, %f module:%s name:%s", x, y, z, module.c_str(), name.c_str());
	return 0;
}

//bool RaySphereCollision(glm::vec3 vSphereCenter, float fSphereRadius, glm::vec3 vA, glm::vec3 vB)
//{
//// Create the vector from end point vA to center of sphere
//glm::vec3 vDirToSphere = vSphereCenter - vA;
//
//// Create a normalized direction vector from end point vA to end point vB
//glm::vec3 vLineDir = glm::normalize(vB-vA);
//
//// Find length of line segment
//float fLineLength = glm::distance(vA, vB);
//
//// Using the dot product, we project the vDirToSphere onto the vector vLineDir
//float t = glm::dot(vDirToSphere, vLineDir);
//
//glm::vec3 vClosestPoint;
//// If our projected distance from vA is less than or equal to 0, the closest point is vA
//if (t <= 0.0f)
//  vClosestPoint = vA;
//// If our projected distance from vA is greater thatn line length, closest point is vB
//else if (t >= fLineLength)
//  vClosestPoint = vB;
//// Otherwise calculate the point on the line using t and return it
//else
//  vClosestPoint = vA+vLineDir*t;
//
//// Now just check if closest point is within radius of sphere
//return glm::distance(vSphereCenter, vClosestPoint) <= fSphereRadius;
//}
