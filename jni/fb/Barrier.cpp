#include "Barrier.h"
#include "Shader.h"

Barrier::Barrier(MeshNodeData* data, Shader* shader):MeshNode(data, shader)
{
}

int Barrier::setPosition(Vector2 v)
{
	MeshNode::setPosition(v);

	left[0] = GV(-mesh->width + v.x,mesh->height + v.y);
	left[1] = GV(-mesh->width + v.x,-mesh->height + v.y);
	right[0] = GV(mesh->width + v.x,mesh->height + v.y);
	right[1] = GV(mesh->width + v.x,-mesh->height + v.y);
	top[0] = GV(mesh->width + v.x,mesh->height + v.y);
	top[1] = GV(-mesh->width + v.x,mesh->height + v.y);
	bottom[0] = GV(mesh->width + v.x,-mesh->height + v.y);
	bottom[1] = GV(-mesh->width + v.x,-mesh->height + v.y);

	return 0;
}

int Barrier::setScale(Vector2 v)
{
	MeshNode::setScale(v);

	left[0] = GV(-mesh->width*v.x + mModelMatrix.v[12],mesh->height*v.y + mModelMatrix.v[13]);
	left[1] = GV(-mesh->width*v.x + mModelMatrix.v[12],-mesh->height*v.y + mModelMatrix.v[13]);
	right[0] = GV(mesh->width*v.x + mModelMatrix.v[12],mesh->height*v.y + mModelMatrix.v[13]);
	right[1] = GV(mesh->width*v.x + mModelMatrix.v[12],-mesh->height*v.y + mModelMatrix.v[13]);
	top[0] = GV(mesh->width*v.x + mModelMatrix.v[12],mesh->height*v.y + mModelMatrix.v[13]);
	top[1] = GV(-mesh->width*v.x + mModelMatrix.v[12],mesh->height*v.y + mModelMatrix.v[13]);
	bottom[0] = GV(mesh->width*v.x + mModelMatrix.v[12],-mesh->height*v.y + mModelMatrix.v[13]);
	bottom[1] = GV(-mesh->width*v.x + mModelMatrix.v[12],-mesh->height*v.y + mModelMatrix.v[13]);

	return 0;
}

int Barrier::Render()
{
	mShader->RenderMesh(this);

	return 0;
}

bool Barrier::CheckCollision(Bird* bird)
{
	Vector2 bp = bird->GetPsition();

	if(distance_Point_to_Segment(bp, left[0], left[1]) < 2)
	{
		bird->restart();
		return true;
	}
	else if(distance_Point_to_Segment(bp, right[0], right[1]) < 2)
	{
		bird->restart();
		return true;
	}
	else if(distance_Point_to_Segment(bp, top[0], top[1]) < 2)
	{
		bird->restart();
		return true;
	}
	else if(distance_Point_to_Segment(bp, bottom[0], bottom[1]) < 2)
	{
		bird->restart();
		return true;
	}

	return false;
}

bool Barrier::CheckCull(Vector2 center, float xRes, float yRes, float c)
{
	Vector2 pos = GetPsition();
	if(pos.x < (center.x - xRes*c - mesh->width) || pos.x > (center.x + xRes*c + mesh->width))
	{
		return true;
	}
	else if(pos.y < (center.y - yRes*c - mesh->height) || pos.y > (center.y + yRes*c + mesh->height))
	{
		return true;
	}

	return false;
}
