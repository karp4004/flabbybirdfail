#include "Shader.h"
#include "Logger.h"
#include "Math/GEMath.h"

GLuint loadShader(GLenum shaderType, const char* pSource) {
    GLuint shader = glCreateShader(shaderType);
    if (shader) {
        glShaderSource(shader, 1, &pSource, NULL);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                char* buf = (char*) malloc(infoLen);
                if (buf) {
                    glGetShaderInfoLog(shader, infoLen, NULL, buf);
                    LOGE("Could not compile shader %x:\n%s\n",
                            shaderType, buf);
                    free(buf);
                }
                glDeleteShader(shader);
                shader = 0;
            }
        }
    }
    return shader;
}


GLuint CompileSource(const char* vertex, const char* fragment)
{
	LOGT("", "");

	GLuint vertexShader = loadShader(GL_VERTEX_SHADER, vertex);
	if (!vertexShader) {
		return 0;
	}

	LOGD("%s", "CompileSource");

	GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, fragment);
	if (!pixelShader) {
		return 0;
	}

	LOGD("%s", "CompileSource");

	GLuint mProgram = glCreateProgram();
	if (mProgram) {
		glAttachShader(mProgram, vertexShader);
		glAttachShader(mProgram, pixelShader);

		glLinkProgram(mProgram);
		GLint linkStatus = GL_FALSE;
		glGetProgramiv(mProgram, GL_LINK_STATUS, &linkStatus);
		if (linkStatus != GL_TRUE) {
			GLint bufLength = 0;
			glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &bufLength);
			if (bufLength) {
				char* buf = (char*) malloc(bufLength);
				if (buf) {
					glGetProgramInfoLog(mProgram, bufLength, NULL, buf);
					LOGE("Could not link mProgram:\n%s\n", buf);
					free(buf);
				}
			}
			glDeleteProgram(mProgram);
			mProgram = 0;
		}
	}


	return mProgram;
}

Shader::Shader()
{
	mBytesPerFloat = 4;
	mPositionDataSize = 3;
	mUVDataSize = 2;
	mNormalDataSize = 3;
}

int Shader::onSurfaceCreated()
{
	const char vertexShaderSource[] =
		  "uniform mat4 u_MVPMatrix;      \n"

		  "attribute vec4 a_Position;     \n"
		  "attribute vec2 aUV;        \n"

		  "varying vec2 vTexCoord;	       \n"

			"void main()  "
			"{                  \n"
			"   vTexCoord = aUV;\n"
			"   gl_Position = u_MVPMatrix * a_Position;                            \n"
			"}\n";

	const char fragmentShaderSource[] =
			"precision mediump float;\n"

			"uniform sampler2D uSampler2D;\n"
			"varying vec2 vTexCoord;	\n"

			"void main (void)\n"
			"{\n"
			"	vec4 sample = texture2D(uSampler2D, vTexCoord);\n"
			"    gl_FragColor = vec4(sample.r, sample.g, sample.b, sample.a);\n"
			"}\n";

	mProgram = CompileSource(vertexShaderSource, fragmentShaderSource);

	mMVPMatrixHandle = glGetUniformLocation(mProgram, "u_MVPMatrix");
	mSampler2D = glGetUniformLocation(mProgram, "uSampler2D");

	return 0;
}

void Shader::setViewMatrix(Vector3 eye, Vector3 center, Vector3 up)
{
	mViewMatrix = getLookAtM(eye, center, up);
}

int Shader::onSurfaceChanged(int l, int t, int width, int height, float coeff)
{
	float left = -width*coeff;
	float right = width*coeff;
	float bottom = -height*coeff;
	float top = height*coeff;
	float near = 1.0f;
	float far = 1000.0f;
	mProjectionMatrix = getOrtho(left, right, bottom, top, near, far);

	return 0;
}

void Shader::RenderMesh(MeshNode* mesh)
{
	mPositionHandle = glGetAttribLocation(mProgram, "a_Position");
	mUVHandle = glGetAttribLocation(mProgram, "aUV");

	glUseProgram(mProgram);

	glVertexAttribPointer(mPositionHandle, mPositionDataSize, GL_FLOAT, false,
			0, mesh->getVB()->positions);

	glEnableVertexAttribArray(mPositionHandle);

	glVertexAttribPointer(mUVHandle, mUVDataSize, GL_FLOAT, false,
			0, mesh->getVB()->uvs);
	glerror("normals");

	glEnableVertexAttribArray(mUVHandle);
	glerror("mNormalHandle");

	mMVMatrix = matrixMultiply(mViewMatrix, mesh->pose());
	mMVPMatrix = matrixMultiply(mProjectionMatrix, mMVMatrix);

	glUseProgram(mProgram);

	mesh->getVB()->mTexture.get()->Bind(mSampler2D);

	glUniformMatrix4fv(mMVPMatrixHandle, 1, false, (float*)mMVPMatrix.v);
	glerror("mMVPMatrix");

	glDrawArrays(GL_TRIANGLES, 0, mesh->getVB()->tris);
	glerror("glDrawArrays");
}
