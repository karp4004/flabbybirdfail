#include <jni.h>

#include <assert.h>

#include <sys/types.h>

#include "PosixTimer.h"

#include "FlabbyBird.h"

#define INFO 1
#define DEBUG 1
#define TRACE 1
#include "Logger.h"

using namespace std;

FlabbyBird app;

extern "C" {
	JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onCreate(JNIEnv * env, jobject obj,  jobject assetManager);
	JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onPause();
	JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onResume();
	JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onRestart();
	JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onStart();
	JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onStop();
	JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onDestroy();
	JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onBackPressed();

	JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onSurfaceCreated(JNIEnv * env, jobject obj);
    JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onSurfaceChanged(JNIEnv * env, jobject obj,  jint width, jint height);
    JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onDrawFrame(JNIEnv * env, jobject obj);

    JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onTouchMove(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jid);

    JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onTouchUp(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jidx);
    JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onTouchDown(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jidx);
};



JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onCreate(JNIEnv * env, jobject obj,  jobject assetManager)
{
	LOGT("", "");

    return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onPause()
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onResume()
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onRestart()
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onStart()
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onStop()
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onDestroy()
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onBackPressed()
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onSurfaceCreated(JNIEnv * env, jobject obj)
{
	LOGT("", "");

	return app.onSurfaceCreated();
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onSurfaceChanged(JNIEnv * env, jobject obj,  jint width, jint height)
{
	LOGT("", "");

	return app.onSurfaceChanged(0, 0, width, height);
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onDrawFrame(JNIEnv * env, jobject obj)
{
	return app.onDrawFrame();
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onTouchMove(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jid)
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onTouchUp(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jid)
{
	LOGT("", "");

	return 0;
}

JNIEXPORT int JNICALL Java_gollardengine_jni_FBJNI_onTouchDown(JNIEnv * env, jobject obj, jfloat x, jfloat y, jint jid)
{
	LOGT("", "");

	return app.onTouch();
}
