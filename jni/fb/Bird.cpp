#include "Bird.h"
#include "Shader.h"

Bird::Bird(MeshNodeData* data, Shader* shader):MeshNode(data, shader),height(0.),yspeed(0.),acc(-30), offset(0.), force_acc(0.)
{
}

int Bird::Render(float dt)
{
    yspeed = yspeed + (force_acc + acc)*dt;
    height = height + yspeed*dt;

    if(force_acc > 0)
    {
    	force_acc = 0;
    }

    if(height < -13. || height > 13.)
    {
    	restart();
    }

	offset += dt*4;

	if(offset > 300)
	{
		restart();
	}

	mModelMatrix = matrixIdentity();
	mModelMatrix = translateM(mModelMatrix, offset, height, 0.0f);
	Vector3 eye = GV(mModelMatrix.v[12], 0, 10);
	Vector3 center = GV(mModelMatrix.v[12], 0, 0);
	Vector3 up = GV(0.,1.,0.);
	mShader->setViewMatrix(eye, center, up);

	mShader->RenderMesh(this);

	return 0;
}

int Bird::restart()
{
	height = 0.;
	offset = 0.;
	yspeed = 0.;
	force_acc = 0.;

	return 0;
}

int Bird::onTouch()
{
	force_acc = 700.;
	yspeed = 0.;

	return 0;
}
