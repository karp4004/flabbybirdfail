
#if defined(_WIN32)
#include <stdio.h>
#define ANDROID_LOG_INFO
#define ANDROID_LOG_DEBUG
#define ANDROID_LOG_WARNING
#define ANDROID_LOG_ERROR
#define log_prefix fprintf(stderr
#define LF "\n"
#define LOG_NAME
#define LOG_FLUSH fflush(stderr);
extern long pthread_self();
#else
#include <android/log.h>
#define log_prefix __android_log_print(
#define LF
#define LOG_NAME "GollardLog",
#define LOG_FLUSH
#endif //_WIN32

#define FOFFSET 0

#ifndef LOG_COLOR
#define LOG_COLOR "#000000"
#endif

static int line_number = 0;

int log_raw(char* buf, int len);

#if INFO
#define  LOGI(fmt, ...)  log_prefix ANDROID_LOG_INFO, LOG_NAME LOG_COLOR " %ld line:%d I %s:%s:%d: " #fmt LF,  pthread_self(), ++line_number, &__FILE__[FOFFSET], __FUNCTION__, __LINE__, __VA_ARGS__); LOG_FLUSH
#else
#define  LOGI(fmt, ...)
#endif

#if DEBUG
#define  LOGD(fmt, ...)  log_prefix ANDROID_LOG_DEBUG, LOG_NAME LOG_COLOR " %ld line:%d D %s:%s:%d: " #fmt LF,  pthread_self(), ++line_number, &__FILE__[FOFFSET], __FUNCTION__, __LINE__, __VA_ARGS__); LOG_FLUSH
#define  LOGOBJECT(object, module, name) object.Log(module, name);
#else
#define  LOGD(fmt, ...)
#define  LOGOBJECT(object, module, name)
#endif

#if WARNING
#define  LOGW(fmt, ...)  log_prefix ANDROID_LOG_WARNING, LOG_NAME LOG_COLOR " %ld line:%d W %s:%s:%d: " #fmt LF,  pthread_self(), ++line_number, &__FILE__[FOFFSET], __FUNCTION__, __LINE__, __VA_ARGS__); LOG_FLUSH
#else
#define  LOGW(fmt, ...)
#endif

#if ERROR
#define  LOGE(fmt, ...)  log_prefix ANDROID_LOG_ERROR, LOG_NAME LOG_COLOR " %ld line:%d E %s:%s:%d: " #fmt LF,  pthread_self(), ++line_number, &__FILE__[FOFFSET], __FUNCTION__, __LINE__, __VA_ARGS__); LOG_FLUSH
#else
#define  LOGE(fmt, ...)
#endif

#if TRACE
#define  LOGT(module, name)  log_prefix ANDROID_LOG_DEBUG, LOG_NAME LOG_COLOR " %ld line:%d T %s:%s:%d name:%s" LF,  pthread_self(), ++line_number, &__FILE__[FOFFSET], __FUNCTION__, __LINE__, name); LOG_FLUSH
#else
#define  LOGT(module, name)
#endif
