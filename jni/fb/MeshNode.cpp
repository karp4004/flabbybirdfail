#include "MeshNode.h"
#include "Shader.h"

MeshNode::MeshNode(MeshNodeData* data, Shader* shader):
mesh(data)
,mShader(shader)
{
	mModelMatrix = matrixIdentity();
}

Vector2 MeshNode::GetPsition()
{
	return GV(mModelMatrix.v[12], mModelMatrix.v[13]);
}

MeshNodeData* MeshNode::getVB()
{
	return mesh;
}

int MeshNode::setPosition(Vector2 v)
{
	mModelMatrix.v[12] = v.x;
	mModelMatrix.v[13] = v.y;

	return 0;
}

int MeshNode::setScale(Vector2 v)
{
	Matrix4 m = matrixScale(v.x, v.y, 1.);

	mModelMatrix = matrixMultiply(mModelMatrix, m);

	return 0;
}

Matrix4& MeshNode::pose()
{
	return mModelMatrix;
}
