#include "FlabbyBird.h"
#include "stdlib.h"
#include "Logger.h"
#include "rc.h"

#define DEBUG 1
#define INFO 1


int FlabbyBird::test_init_quad(MeshNodeData* mesh, float xscale, float yscale)
{
	float cubePositionData[] =
	{
			// Front face
			-xscale, yscale, 0,
			-xscale, -yscale, 0,
			xscale, yscale, 0,
			-xscale, -yscale, 0,
			xscale, -yscale, 0,
			xscale, yscale, 0,
	};

	float cubeNormalData[] =
	{
			// Front face
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
	};

	float cubeTex[] =
   {
	  0.0f, 0.0f,
	  0.0f, 1.0f,
	  1.0f, 0.0f,
	  0.0f, 1.0f,
	  1.0f, 1.0f,
	  1.0f, 0.0f,
   };

	LOGD("%s", "test_init 1");

	mTimer.reset(new PosixTimer("PosixTimer"));

	LOGD("%s", "test_init 2");

	LOGD("%s", "test_init 3");

	mesh->positions = (float*)malloc(sizeof(cubePositionData));
	memcpy(mesh->positions, cubePositionData, sizeof(cubePositionData));

	LOGD("%s", "test_init 4");

	mesh->normals = (float*)malloc(sizeof(cubeNormalData));
	memcpy(mesh->normals, cubeNormalData, sizeof(cubeNormalData));

	LOGD("%s", "test_init 5");

	mesh->uvs = (float*)malloc(sizeof(cubeTex));
	memcpy(mesh->uvs, cubeTex, sizeof(cubeTex));

	mesh->tris = 6;
	mesh->width = 3;
	mesh->height = 3;
}


FlabbyBird::FlabbyBird():
proj_coeff(0.02)
{
	test_init_quad(&barrier_data, 3, 3);
	test_init_quad(&bird_data, 2, 2);

	for(int i=2;i<100;i++)
	{
		shared_ptr<Barrier> barrier(new Barrier(&barrier_data, &shader));
		barrier->setPosition(GV(i*13, 0));
		barriers.push_back(barrier);
	}

	bird.reset(new Bird(&bird_data, &shader));
}

int FlabbyBird::onSurfaceCreated()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(false);
	glEnable(GL_BLEND);

	shader.onSurfaceCreated();

    barrier_data.mTexture.reset(new Texture(barrier_tex, sizeof(barrier_tex), 2, 2));

	bird_data.mTexture.reset(new Texture(bird_tex, sizeof(bird_tex), 16, 16));

	return 0;
}

int FlabbyBird::onSurfaceChanged(int l, int t, int width, int height)
{
    xRes = width;
    yRes = height;

    static bool inited = false;

    if(!inited)
    {
		bool par = false;
		vector<shared_ptr<Barrier>>::iterator it = barriers.begin();
		while(it != barriers.end())
		{
			Vector2 pos = it->get()->GetPsition();

			pos.y = yRes*proj_coeff;

			if(!par)
			{
				pos.y = -pos.y;
			}

			par = !par;

			float rsc = rand() % 5 + 2;

			it->get()->setPosition(pos);
			it->get()->setScale(GV(0.5,rsc));

			it++;
		}

		inited = true;
    }

	glViewport(0, 0, width, height);
	glerror("glViewport");

	shader.onSurfaceChanged(l, t, width, height, proj_coeff);

	return 0;
}

int FlabbyBird::onTimer()
{
	mTimer->ResetElapsed();
	return 0;
}

int FlabbyBird::onDrawFrame()
{
	mTimer->Update();
	double dt = mTimer->GetDelta();

	if(mTimer->GetElapsed() >= 1.0)
	{
		onTimer();
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	bird->Render(dt);

    vector<shared_ptr<Barrier>>::iterator it = barriers.begin();
    while(it != barriers.end())
    {
    	Vector2 c = GV(bird->GetPsition().x, 0.);
    	if(!it->get()->CheckCull(c, xRes, yRes, proj_coeff))
    	{
			it->get()->Render();
			if(it->get()->CheckCollision(bird.get()))
			{
				break;
			}
    	}

    	it++;
    }

	return 0;
}

int FlabbyBird::onTouch()
{
	bird->onTouch();

	return 0;
}
