#ifndef TEXTUREH
#define TEXTUREH

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

int glerror(const char* op);

class Texture
{
public:
	Texture(unsigned char* d, int sz, int w, int h);
	int Allocate();
	int Bind(GLint mSampler2D);

private:
	GLuint mId;
	GLushort mBpp;
	GLushort mFormat;
	GLushort mUnit;
	GLushort mCap;

	int mWidth;
	int mHeight;
	int mDataSize;
	unsigned char* mData;
};

#endif
