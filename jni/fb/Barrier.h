#ifndef BARRIERH
#define BARRIERH

#include "MeshNode.h"
#include "Math/Vector2.h"
#include "Bird.h"

class Shader;

class Barrier: public MeshNode
{
public:
	Barrier(MeshNodeData* data, Shader* shader);
	int Render();
	bool CheckCollision(Bird* b);
	bool CheckCull(Vector2 center, float xRes, float yRes, float c);
	virtual int setPosition(Vector2 v);
	virtual int setScale(Vector2 v);

private:
	Vector2 left[2];
	Vector2 right[2];
	Vector2 top[2];
	Vector2 bottom[2];
};

#endif
