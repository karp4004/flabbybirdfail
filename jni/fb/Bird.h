#ifndef BIRDH
#define BIRDH

#include "MeshNode.h"

class Shader;

class Bird: public MeshNode
{
public:
	Bird(MeshNodeData* data, Shader* shader);
	int Render(float dt);
	int restart();
	int onTouch();

private:
	float height;
    float offset;
	float yspeed;
	float acc;
	float force_acc;
};

#endif
