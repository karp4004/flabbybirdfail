#include "Timer.h"

#include "Logger.h"

Timer::Timer(string name)
:mElapsed(0.)
,mDelta(0.)
{
}

double Timer::GetElapsed()
{
	return mElapsed;
}

double Timer::GetDelta()
{
	return mDelta;
}

void Timer::CountElapsed(double t)
{
	mElapsed += t;
}

void Timer::ResetElapsed()
{
	mElapsed = 0.;
}

void Timer::SetDelta(double t)
{
	mDelta = t;
}

int Timer::Reset()
{
	mElapsed = 0.;
	mDelta = 0.;

	return 0;
}
