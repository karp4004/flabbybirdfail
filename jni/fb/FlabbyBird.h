#ifndef FlabbyBirdH
#define FlabbyBirdH

#include "Math/GEMath.h"
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <bits/extc++.h>

#include "Shader.h"
#include "PosixTimer.h"
#include "MeshNode.h"
#include "Barrier.h"
#include "Bird.h"

class FlabbyBird
{
public:
	FlabbyBird();

	int onSurfaceCreated();
	int onSurfaceChanged(int left, int top, int width, int height);
	int onDrawFrame();
	int onTouch();
	int restart();

private:

	int test_init_quad(MeshNodeData* mesh, float scale, float yscale);
	GLuint loadShader(GLenum shaderType, const char* pSource);
	GLuint CompileSource(const char* vertex, const char* fragment);
	void setAttributes(int program, MeshNodeData* mesh);
	int onTimer();


    vector<shared_ptr<Barrier>> barriers;
    shared_ptr<Bird> bird;

    MeshNodeData bird_data;
    MeshNodeData barrier_data;

    Shader shader;

    shared_ptr<Timer> mTimer;

    int xRes;
    int yRes;
    float proj_coeff;
};

#endif
