package gollardengine.jni;

import android.content.res.AssetManager;

public class FBJNI {
    static {
        System.loadLibrary("FBJNI");
    }

   /**
    * @param width the current view width
    * @param height the current view height
    */
    public static native int onCreate(AssetManager am);
    public static native  void onRestart();
    public static native void onStart();
    public static native  void onStop();
    public static native void onDestroy();
    public static native  void onPause();
    public static native void onResume();
    public static native void onBackPressed();
    
    public static native void onSurfaceCreated();
    public static native void onSurfaceChanged(int width, int height);
    public static native void onDrawFrame();
    
    public static native void onTouchMove(float x, float y, int idx);
    public static native void onTouchUp(float x, float y, int idx);
    public static native void onTouchDown(float x, float y, int idx);
}
