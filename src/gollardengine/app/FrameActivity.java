package gollardengine.app;

import gollardengine.jni.FBJNI;
import gollardengine.util.SystemUiHider;
import android.app.Activity;
import android.os.Bundle;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class FrameActivity extends Activity {
	
	FrameView mView;
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FBJNI.onCreate(getAssets());
		
        mView = new FrameView(getApplication(), false, 8, 0);
        setContentView(mView);
	}

	@Override public void onBackPressed()
	{
		super.onBackPressed();
		FBJNI.onBackPressed();
	}
	
	@Override protected void onRestart()
	{
		super.onRestart();
		FBJNI.onRestart();
	}
	
	@Override protected void onStart()
	{
		super.onStart();
		FBJNI.onStart();
	}
	
	@Override protected void onStop()
	{
		super.onStop();
		FBJNI.onStop();
	}
	
	@Override protected void onDestroy()
	{
		super.onDestroy();
		FBJNI.onDestroy();
		
		System.exit(0);
	}
	
    @Override protected void onPause() {
        super.onPause();
        mView.onPause();
        
        FBJNI.onPause();
    }

    @Override protected void onResume() {
        super.onResume();
        mView.onResume();
        
        FBJNI.onResume();
    }
}
